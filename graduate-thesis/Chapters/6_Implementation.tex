\lhead{\emph{Implementation}}
\chapter{Implementation}

\quad\quad In this chapter, we discuss about the implementation of machine learning techniques to distinguish normal connections from attacks using various datasets. First and foremost, we use KDD 1999 cup dataset and see its drawbacks. Then we look at a subset of KDD dataset, which alleviates many of KDD's problems, called NSL-KDD. Finally, we explore a newer dataset, UNSW NB15, as an alternative to the de-facto standard of academia, KDD 1999 cup dataset. We use the exact same techniques across all the three datasets so as to have common grounds for comparison. For all datasets, we perform two sets of classification. First, a multiclass classification for all classes present in the dataset and second, a binary classification, i.e., attack vs normal.

\quad\quad There has been extensive research on KDD and NSL-KDD datasets and particularly on their pitfalls. Hence, we do not use those datasets as is but take measures to counter some of their problems.

\quad\quad In the KDD dataset, each of the DoS attacks introduces several data points to the dataset. The overwhelming majority of DoS introduces a strong bias towards it, with a null rate of 78.3\% (3,883,370 out of 4,898,439). We filter out all duplicates from KDD’s training set, keeping one representative example for the bag of duplicates; this greatly reduces its size to 1,074,992, with a DoS null rate of 23\%. There has been a lot of research using the complete KDD dataset, hence, we used KDD without duplicates and yet the results do not vary significantly.

\quad\quad NSL-KDD still suffers from the problem of skewed class distributions  \cite{Reference28}. So to determine whether we get improved results if we have balanced distributions, we oversample the minority classes and then undersample examples from all classes to have equal number of samples from each class in the final dataset. The oversampling technique used is not the simple oversampling by replacement as it results in uoverfitting. We use a technique called SMOTE(Synthetic Minority Oversampling Technique). This technique operates in the feature space rather than the data space. In SMOTE, a sample from the minority class is selected and line segments are drawn to its K-Nearest Neighbours. Each point on each such segment is a new sample of the minority class  \cite{Reference29}. We use different samples so as to keep the variance of the feature high. So for NSL-KDD, we perform three sets of classification:
\begin{enumerate}
	\setcounter{enumi}{0} %% Source: http://tex.stackexchange.com/a/149/110560
	%% Source for \input{...} command: http://tex.stackexchange.com/a/250/110560
	\item NSL-KDD Multiclass classification
	\item NSL-KDD Binary classification
	\item NSL-KDD with SMOTE + Random Undersampling Multiclass classification
\end{enumerate}

\section{Preprocessing and Feature Reduction}
\quad\quad In this section we explore the different methods of cleaning and reducing the features of the various datasets, based on their own distinctive properties. 

\subsection{Feature Scaling}
\quad\quad A raw dataset can never be fed directly into a machine learning model. Certain data cleaning and preprocessing steps are always necessary. Data cleaning primarily includes handling null/missing values while preprocessing transforms the data such that it is viable to be given as input to a model.

\quad\quad Machine learning models strictly want numerical input. In all our three datasets(KDD, NSL-KDD and UNSW NB15), we have a few features that have string values so they are converted to numerical values. But since they are categorical attributes, there are many ways to convert them to numerical values, but each one of them has a few pros and cons. One Hot encoding and Discretization are the more commonly used methods. One Hot encoding is a more accurate representation of categorical attributes but causes feature space explosion while Discretization gives a succinct representation but implies an ordering in the values where none exist. Since our data is already high dimensional, we used Discretization. 

\quad\quad E.g.: \{\textit{Paris}, \textit{Paris}, \textit{Tokyo}, \textit{Amsterdam}\} $\Rightarrow$ \{1, 1, 2, 0\}

\quad\quad A major step in preprocessing, is to scale the data. Scaling is of utmost importance because otherwise a feature that has higher values will overshadow all the other features thus resulting in a very poor model. 

\quad\quad The two most common scaling techniques are: 	
\vspace{-1.5em}	
\begin{enumerate}
	\item Normalization
	\vspace{-0.75em}	
	\item Standardization
\end{enumerate}

\quad\quad \textbf{Normalization} centers the data around the minimum of the distribution and scales by \textit{max} - \textit{min} and hence results in a range of [0.0, 1.0]. 

\hspace{5em} Normalization:
\begin{equation}
x_{i, 0...1} = 
\frac	{ x_{i} - x_{min}	}
{ x_{max} - x_{min}	}
\end{equation}

\quad\quad \textbf{Standardization} centers the data around the mean of the distribution and scales by the standard deviation. A data point that is standardized is also called the z-score the data.

\quad\quad Standardization gives the number of standard deviations from the mean. Thus it generally results in a range of [-1.0, 1.0] but in actuality it can be [$-\infty$, $\infty$]. Higher the value the more far the value from mean. 1 standard deviation can explain only around 68\% variance. It is generally found that models perform better using standardization. Thus in all the datasets we use standardization. Figure \ref{fig: Gaussian curve} shows the distribution.

\hspace{5em} Standardization:
\begin{equation}
x_{i, \sigma} = 
\frac	{ x_{i} - \bar{x}_{s}} 
{ \sigma_{x,s} 		 }
\end{equation}


\begin{figure}[!h]
	\centerline{\includegraphics[scale=0.7]{./images/normal_distribution.jpg}}
	\caption{A Gaussian curve.}
	\label{fig: Gaussian curve}
\end{figure}

\subsection{Feature Reduction}
\quad\quad High dimensional data causes overfitting of data: in high dimensions, points are farther than they would in lower dimensions, causing Euclidean distance measures to perform poorly in algorithms such as clustering. This is known as the \textit{Curse of Dimensionality}, as increasing features can lead to better predictions but also increases the variance as well. 

\quad\quad Since all of our datasets have a relatively high number of predictors, we use feature reduction to mitigate the Curse of Dimensionality. There are a plethora of feature reduction techniques: \textit{Forward Feature selection}, \textit{Variance thresholding}, \textit{PCA} etc. We have used a tree based approach to feature reduction, as discussed below.

\quad\quad Decision trees generally learn important features near the top of the tree, which can be used for rough feature selection. Extending this idea, we can obtain the relative feature importance of each feature using a Random Forest classifier and the \textit{Mean Decrease Accuracy} metric. The notion behind this metric is: by randomly permuting the values of each feature across the training examples, and measuring the corresponding change in the \textit{Out-of-Bag error} (or any other accuracy metric), we are able to determine which features affect accuracy. Clearly, a feature $x_i$ is relatively unimportant if the accuracy of a RandomForest model obtained by permuting the values of $x_i$ does not significantly deviate from the accuracy of the non-permuted RandomForest. Greater the decrease in accuracy, the more important the feature.

\quad\quad Unfortunately, the above technique operates slowly for large feature-sets such as our own. Instead, we employ a proxy method which measures the \textit{Mean Decrease Impurity} (MDI). For each node that was split on a feature $x_i$, the decrease in Gini-index (or Cross-entropy) from the original node to that of the two regions is calculated, and then summed up for each such node in the entire tree, weighted by the number of examples. Finally, these values are averaged over all \textit{B} trees in the to deliver the MDI. Higher is this weighted decrease in impurity, higher is the importance. The advantage of this method is that it can be used for decision trees as well. 

\quad\quad For each of our datasets, we have eliminated features that have extremely low importances, of the order of $10^{-3}$ or less. The MDI elimination has been corroborated by using Variance Thresholding and Forward Feature selection.

\clearpage

\quad\quad In this section we describe the particulars of the classifiers used on each dataset.

\quad\quad We show the implementations model-wise. The same model is assumed to be trained on each dataset, though the exact values of the hyperparameters may differ as they are selected via 3-fold Cross Validation. Any other differences in the model used are explicitly mentioned.


\section{Decision Tree:}

\begin{figure}[!h]
 \hspace{-6em}{\includegraphics[scale=0.5]{./images/Flowcharts/Decision_Trees.png}}
	\caption{A flowchart for a decision tree.}
	\label{fig: Decision Tree flowchart}
\end{figure}


\quad\quad A decision tree classifier is a model which learns by splitting the feature-space at right angles to the axes. We build the tree by using recursive splitting at each of  the leaves, until the leaf is either pure (i.e. all of its examples are from one class) or we have reached the stopping criterion (maximum depth, etc.). At every point, the ‘best split’ is taken, decided by selecting the value of the feature which leads to the largest decrease in the Gini index or cross-entropy (calculated for each of the regions separately). Figure \ref{fig: Decision Tree flowchart} shows the flowchart for implementation of decision tree.



\quad\quad The trees considered here use Gini index to decide the best split. The choice of split function makes a negligible difference in the growth of tree as most of the impurities are consistent with each other  \cite{Reference27}. Here we have used Gini index over entropy as the former is faster and better at minimizing misclassification error. We do not perform early stopping. In general, tree pruning may be undergone to reduce the overfitting, but we do not consider that here. In the case of the reduced feature-set, we limit the depth of the tree to reduce overfitting. Since the classes are highly skewed, the decision tree learns some biases towards the ‘Normal’ and ‘DoS’ classes.






\section{Random Forest}

\begin{figure}[!h]
	\hspace{-2em}{\includegraphics[scale=0.4]{./images/Flowcharts/Random_Forest.png}}
	\caption{A flowchart for training a RandomForest model.}
	\label{fig: Random Forest flowchart}
\end{figure}

\quad\quad The Random Forest model, proposed by Brierman in 2001, overcomes several of the disadvantages of Decision trees through bagging and randomly-built Decision Trees.

\quad\quad Bagging, or Bootstrap Aggregating, is the process in which multiple Decision Trees are built using the bootstrap: random subsamples of the training set with replacement. This allows us to reduce the variance of the any statistical model; for prediction, we consider the most popular vote among B trees.

\quad\quad Additionally in a Random Forest model, each tree is randomly built: at each recursive binary split, the best feature is chosen among a random subset of the features. m out of a total of p features are randomly selected and the best feature and split point is selected, at every split of a leaf node. This procedure allows us to decorrelate the trees, as with simple bagging it would be possible for a very strong predictor to dominate and introduce bias in each of the trees. Figure \ref{fig: Random Forest flowchart} shows the flowchart for random forest implementation. Averaging many uncorrelated quantities leads to  a larger decrease in variance than averaging correlated quantities.

\quad\quad As with other bagging models, we do not overfit by reasonably increasing the number of trees. We limit the depth of the tree and the number of trees, to reduce overfitting. Here again we use Gini Index as the split criterion.





\section{Neural Networks}


\begin{figure}[!h]
	\hspace{-6em}{\includegraphics[scale=0.4]{./images/Flowcharts/Neural_Networks.png}}
	\caption{A flowchart for training a Neural Network.}
	\label{fig: ANN flowchart}
\end{figure}

\quad\quad Supervised Neural Networks in general are multilayer models. Each node is a non-recursive mapping from each of the nodes of the previous layer, with a bias unit added to introduce a learned threshold value. The sum of the products of the weights and the inputs from the previous layer is calculated and then limited by the activation function. This is repeated from the first layer (the raw inputs) upto the final layer (the predicted outputs).

\quad\quad The Backpropagation algorithm, used to calculate the error from the outputs and then ‘push back’ the values (using the calculated gradients) is used to update the  weights, for each input example.  Figure \ref{fig: ANN flowchart} shows the flowchart to implement a neural network.

\quad\quad We consider architectures with a homogeneous number of nodes per layer. We have not considered architectures with more than one hidden layer as it leads to overfitting. We regularize using L2 norm. Neural networks take a long time to train, thus to reduce this, we stop training when the error rate does not reduce significantly. 

\quad\quad The activation function used is ReLu(Rectified Linear unit) which is given by $h = max(0, a)$ where $a = Wx + b$. Since it converts negative values to 0, ReLu gives a sparse representation of the output. Also, ReLu results in faster convergence than sigmoid activation.




\section{Naive Bayes}

\begin{figure}[!h]
	\hspace{-6em}{\includegraphics[scale=0.4]{./images/Flowcharts/Naive_Bayes.png}}
	\caption{A flowchart for training a Naive Bayes classifier.}
	\label{fig: Naive Bayes flowchart}
\end{figure}

\quad\quad Naive Bayes is an extremely fast, scalable model which allows us to directly calculate the posterior probability $P(Y = k | X = x_1,x_2...x_n)$ as the product of the prior probabilities of each of the classes and the likelihood. Figure \ref{fig: Naive Bayes flowchart} shows the flowchart to implement a Naive Bayes classifier.

\quad\quad We make the simplifying assumption that the likelihood $P(X = x_1,x_2\ldots x_n | Y=k)$, which is difficult to estimate, is simply 
\begin{equation}
{\displaystyle \prod_{i=1}^{n} P(X = x_i|Y = k)}
\end{equation}
\quad\quad That is, each of the features are independent given the output class. We then predict using Bayes Rule, and select the class using the Maximum Likelihood Estimate.

\quad\quad Naive Bayes works well for discrete features but cannot be directly applied to continuous features. Thus the probabilities cannot be calculated as the ratio of number of occurences to total number of examples. We model the probabilities as normal distributions. We consider the likelihood $P(X = x_i | Y = k)$ is the probability value obtained from the Gaussian Distribution over that feature $x_i$, for the $k^{th}$ class.




\section{Support Vector Machine}

\quad\quad Support Vector Machines are learning models which select the best separating hyperplane, with a guaranteed margin. We work with soft-margin classifiers, where the fit can be controlled by the float, C. 

\quad\quad The \textit{kernel trick} of an SVM maps the input dataset to a new featurespace where the data is linearly separable, thus allowing us to pipe the input into an SVM which is able to learn the optimal linear separating hyperplane, and return a complex, non-linear separating hyperplane. 

\quad\quad We use the RBF kernel for two purposes: it is an exponential kernel, taking the current feature-space and finding the optimal hyperplane in an infinite dimension space where the data becomes linearly separable, and it is the kernel most widely used in literature. 

\quad\quad SVMs with kernel functions are resource intensive and time consuming to train as well as to test. It is hard to scale to dataset with more than a couple of 10000 samples. Thus, we subsample from the dataset without replacement, choosing at most 25,000 data points from each of the datasets. This corresponds to around 2.5\% of the duplicates-filtered KDD dataset, 19.8\% of NSL, and 14.3\% of UNSW-NB15. But the results are representative of the whole dataset as SVMs require less data due to the kernel trick and also due to the concept of support vectors.




\section{K-Means Clustering for classification}

\quad\quad In addition to the ordinary classification models, we have also used clustering for to classify the examples for each of the datasets. In this technique, the clusters are assigned class labels based on the majority vote in the cluster. When predicting class of test examples, they are assigned a cluster which is then converted to the class label of that cluster. 

\quad\quad We have used Mini-batch KMeans instead of vanilla KMeans as the latter considers the whole dataset at each step, and must be executed multiple times to obtain the best initial centroid allocation, slowing down the execution time significantly. MiniBatch KMeans runs just once and the best centroid allocation is performed at the start of the execution, using an initial sample depending upon the best inertia scores from various random initializations. MiniBatch KMeans updates the centroids after each sample rather than each batch. It doesn’t converge like KMeans but the performance is only marginally poor. Hence for large datasets, such as the ones we deal with, MiniBatch KMeans is used over KMeans at the slight cost of cluster quality.



\clearpage

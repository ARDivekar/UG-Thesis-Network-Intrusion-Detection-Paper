\lhead{\emph{Preprocessing and Feature Reduction}}
\chapter{Preprocessing and Feature Reduction}

\quad\quad In the following section, we discuss about the implementation of machine learning techniques to distinguish normal connections from attacks using various datasets. First and foremost, we use KDD 1999 cup dataset and see its drawbacks. Then we look at a subset of KDD dataset, which alleviates many of KDD's problems, called NSL-KDD. Finally, we explore a newer dataset, UNSW NB15, as an alternative to the de-facto standard of academia, KDD 1999 cup dataset. We use the exact same techniques across all the three datasets so as to have common grounds for comparison. For all datasets, we perform two sets of classification. First, a multiclass classification for all classes present in the dataset and second, a binary classification, i.e., attack vs normal.

\quad\quad There has been extensive research on KDD and NSL-KDD datasets and particularly on their pitfalls. Hence, we do not use those datasets as is but take measures to counter some of their problems.

\quad\quad In the KDD dataset, each of the DoS attacks introduces several data points to the dataset. The overwhelming majority of DoS introduces a strong bias towards it, with a null rate of 78.3\% (3,883,370 out of 4,898,439). We filter out all duplicates from KDD’s training set, keeping one representative example for the bag of duplicates; this greatly reduces its size to 1,074,992, with a DoS null rate of 23\%. There has been a lot of research using the complete KDD dataset, hence, we used KDD without duplicates and yet the results do not vary significantly.

\quad\quad NSL-KDD still suffers from the problem of skewed class distributions\cite{Reference28}. So to determine whether we get improved results if we have balanced distributions, we oversample the minority classes and then undersample examples from all classes to have equal number of samples from each class in the final dataset. The oversampling technique used is not the simple oversampling by replacement as it results in uoverfitting. We use a technique called SMOTE(Synthetic Minority Oversampling Technique). This technique operates in the feature space rather than the data space. In SMOTE, a sample from the minority class is selected and line segments are drawn to its K-Nearest Neighbours. Each point on each such segment is a new sample of the minority class\cite{Reference29}. We use different samples so as to keep the variance of the feature high. So for NSL-KDD, we perform three sets of classification:
\begin{enumerate}
\setcounter{enumi}{0} %% Source: http://tex.stackexchange.com/a/149/110560
%% Source for \input{...} command: http://tex.stackexchange.com/a/250/110560
\item NSL-KDD Multiclass classification
\item NSL-KDD Binary classification
\item NSL-KDD with SMOTE + Random Undersampling Multiclass classification
\end{enumerate}

\quad\quad In this section we explore the different methods of cleaning and reducing the features of the various datasets, based on their own distinctive properties. 


\section{Feature Scaling}
\quad\quad A raw dataset can never be fed directly into a machine learning model. Certain data cleaning and preprocessing steps are always necessary. Data cleaning primarily includes handling null/missing values while preprocessing transforms the data such that it is viable to be given as input to a model.

\quad\quad Machine learning models strictly want numerical input. In all our three datasets(KDD, NSL-KDD and UNSW NB15), we have a few features that have string values so they are converted to numerical values. But since they are categorical attributes, there are many ways to convert them to numerical values, but each one of them has a few pros and cons. One Hot encoding and Discretization are the more commonly used methods. One Hot encoding is a more accurate representation of categorical attributes but causes feature space explosion while Discretization gives a succinct representation but implies an ordering in the values where none exist. Since our data is already high dimensional, we used Discretization. 

\quad\quad E.g.: \{\textit{Paris}, \textit{Paris}, \textit{Tokyo}, \textit{Amsterdam}\} $\Rightarrow$ \{1, 1, 2, 0\}

\quad\quad A major step in preprocessing, is to scale the data. Scaling is of utmost importance because otherwise a feature that has higher values will overshadow all the other features thus resulting in a very poor model. 

\quad\quad The two most common scaling techniques are: 	
\vspace{-1.5em}	
\begin{enumerate}
\item Normalization
\vspace{-0.75em}	
\item Standardization
\end{enumerate}
 
\quad\quad \textbf{Normalization} centers the data around the minimum of the distribution and scales by \textit{max} - \textit{min} and hence results in a range of [0.0, 1.0]. 

\hspace{5em} Normalization:
\begin{equation*}
x_{i, 0...1} = 
\frac	{ x_{i} - x_{min}	}
		{ x_{max} - x_{min}	}
\end{equation*}

\quad\quad \textbf{Standardization} centers the data around the mean of the distribution and scales by the standard deviation. A data point that is standardized is also called the z-score the data.

\quad\quad Standardization gives the number of standard deviations from the mean. Thus it generally results in a range of [-1.0, 1.0] but in actuality it can be [$-\infty$, $\infty$]. Higher the value the more far the value from mean. 1 standard deviation can explain only around 68\% variance. It is generally found that models perform better using standardization. Thus in all the datasets we use standardization.

\hspace{5em} Standardization:
\begin{equation*}
x_{i, \sigma} = 
\frac	{ x_{i} - \bar{x}_{s}} 
		{ \sigma_{x,s} 		 }
\end{equation*}


\begin{figure}[!h]
	\centerline{\includegraphics[scale=0.7]{./images/normal_distribution.jpg}}
	\caption{A Gaussian curve.}
	\label{fig: Gaussian curve}
\end{figure}

\section{Feature Reduction}
\quad\quad High dimensional data causes overfitting of data: in high dimensions, points are farther than they would in lower dimensions, causing Euclidean distance measures to perform poorly in algorithms such as clustering. This is known as the \textit{Curse of Dimensionality}, as increasing features can lead to better predictions but also increases the variance as well. 

\quad\quad Since all of our datasets have a relatively high number of predictors, we use feature reduction to mitigate the Curse of Dimensionality. There are a plethora of feature reduction techniques: \textit{Forward Feature selection}, \textit{Variance thresholding}, \textit{PCA} etc. We have used a tree based approach to feature reduction, as discussed below.

\quad\quad Decision trees generally learn important features near the top of the tree, which can be used for rough feature selection. Extending this idea, we can obtain the relative feature importance of each feature using a Random Forest classifier and the \textit{Mean Decrease Accuracy} metric. The notion behind this metric is: by randomly permuting the values of each feature across the training examples, and measuring the corresponding change in the \textit{Out-of-Bag error} (or any other accuracy metric), we are able to determine which features affect accuracy. Clearly, a feature $x_i$ is relatively unimportant if the accuracy of a RandomForest model obtained by permuting the values of $x_i$ does not significantly deviate from the accuracy of the non-permuted RandomForest. Greater the decrease in accuracy, the more important the feature.

\quad\quad Unfortunately, the above technique operates slowly for large feature-sets such as our own. Instead, we employ a proxy method which measures the \textit{Mean Decrease Impurity} (MDI). For each node that was split on a feature $x_i$, the decrease in Gini-index (or Cross-entropy) from the original node to that of the two regions is calculated, and then summed up for each such node in the entire tree, weighted by the number of examples. Finally, these values are averaged over all \textit{B} trees in the to deliver the MDI. Higher is this weighted decrease in impurity, higher is the importance. The advantage of this method is that it can be used for decision trees as well. 

\quad\quad For each of our datasets, we have eliminated features that have extremely low importances, of the order of $10^{-3}$ or less. The MDI elimination has been corroborated by using Variance Thresholding and Forward Feature selection.

\clearpage

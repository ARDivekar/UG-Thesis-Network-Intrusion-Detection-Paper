%% ----------------------------------------------------------------
%% Thesis.tex -- MAIN FILE (the one that you compile with LaTeX)
%% ---------------------------------------------------------------- 

% Set up the document
\documentclass[a4paper, 11pt, oneside]{Thesis}  % Use the "Thesis" style, based on the ECS Thesis style by Steve Gunn

% Include any extra LaTeX packages required
\usepackage{verbatim}  % Needed for the "comment" environment to make LaTeX comments
\usepackage{vector}  % Allows "\bvec{}" and "\buvec{}" for "blackboard" style bold vectors in maths

\usepackage{graphicx}  % For images, see Neural Networks section for example
\graphicspath{Figures/}  % Location of the graphics files (set up for graphics to be in PDF format)


\usepackage{tabu}
\usepackage{array}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage[tableposition=top]{caption}
\usepackage{enumitem}

\DeclareMathOperator*{\argmax}{argmax}


\usepackage{url}
%\Urlmuskip=0mu plus 1mu\relax  %Used for BibTex. Source: https://tex.stackexchange.com/a/115820/110560

\usepackage{hyperref}
\hypersetup{urlcolor=blue, colorlinks=true}  % Colours hyperlinks in blue. This may be distracting if there are many links.


%% ----------------------------------------------------------------
\begin{document}
	

\frontmatter      % Begin Roman style (i, ii, iii, iv...) page numbering

% Set up the Title Page


\title{
	Benchmarking Novel Datasets For Network Intrusion Detection:  KDD '99 Alternatives
}


\authors {
	Rudra Kumar Mishra\\
	Meet Parekh\\
	Vaibhav Savla\\
	Abhishek Divekar
}

\addresses  {\groupname\\\deptname\\\univname}  % Do not change this here, instead these must be set in the "Thesis.cls" file, please look through it instead
\date       {\today}
\subject    {}
\keywords   {}

\maketitle
%% ----------------------------------------------------------------

\setstretch{1.3}  % It is better to have smaller font and larger line spacing than the other way round

% Define the page headers using the FancyHdr package and set up for one-sided printing
\fancyhead{}  % Clears all page headers and footers
\rhead{\thepage}  % Sets the right side header to show the page number
\lhead{}  % Clears the left side page header

\pagestyle{fancy}  % Finally, use the "fancy" page style to implement the FancyHdr headers

%% ----------------------------------------------------------------
% Declaration Page required for the Thesis, your institution may give you a different text to place here

% Hide declaration page
\iffalse
\Declaration{

\addtocontents{toc}{\vspace{1em}}  % Add a gap in the Contents, for aesthetics

I, AUTHOR NAME, declare that this thesis titled, `THESIS TITLE' and the work presented in it are my own. I confirm that:

\begin{itemize} 
\item[\tiny{$\blacksquare$}] This work was done wholly or mainly while in candidature for a research degree at this University.
 
\item[\tiny{$\blacksquare$}] Where any part of this thesis has previously been submitted for a degree or any other qualification at this University or any other institution, this has been clearly stated.
 
\item[\tiny{$\blacksquare$}] Where I have consulted the published work of others, this is always clearly attributed.
 
\item[\tiny{$\blacksquare$}] Where I have quoted from the work of others, the source is always given. With the exception of such quotations, this thesis is entirely my own work.
 
\item[\tiny{$\blacksquare$}] I have acknowledged all main sources of help.
 
\item[\tiny{$\blacksquare$}] Where the thesis is based on work done by myself jointly with others, I have made clear exactly what was done by others and what I have contributed myself.
\\
\end{itemize}
 
 
Signed:\\
\rule[1em]{25em}{0.5pt}  % This prints a line for the signature
 
Date:\\
\rule[1em]{25em}{0.5pt}  % This prints a line to write the date
}
\fi
% End of iffalse
\clearpage  % Declaration ended, now start a new page

%% ----------------------------------------------------------------
% The "Funny Quote Page"
% Hide funny quote page
\iffalse
\pagestyle{empty}  % No headers or footers for the following pages

\null\vfill
% Now comes the "Funny Quote", written in italics
\textit{``Write a funny quote here.''}

\begin{flushright}
If the quote is taken from someone, their name goes here
\end{flushright}

\vfill\vfill\vfill\vfill\vfill\vfill\null
\fi
% End of iffalse
\clearpage  % Funny Quote page ended, start a new page
%% ----------------------------------------------------------------

\iffalse %Hide acknowledgements page
\setstretch{1.3}  % Reset the line-spacing to 1.3 for body text (if it has changed)

% The Acknowledgements page, for thanking everyone
\acknowledgements{
\addtocontents{toc}{\vspace{1em}}  % Add a gap in the Contents, for aesthetics

The acknowledgements and the people to thank go here, don't forget to include your project advisor\ldots

}
\fi %Close iffalse of acknowledgements page.
\clearpage  % End of the Acknowledgements
%% ----------------------------------------------------------------

% The Abstract Page
\addtotoc{Abstract}  % Add the "Abstract" page entry to the Contents
\abstract{
\thispagestyle{plain}
\addtocontents{toc}{\vspace{1em}}  % Add a gap in the Contents, for aesthetics

\quad Though often cited as difficult to implement in practice, Machine Learning models have been steadily gaining traction for their use in Network Intrusion Detection Systems (NIDS). Research into this domain is frequently performed on the \textit{KDD Cup '99} dataset, a subsampling of simulated network intrusion attacks conducted at DARPA. Several studies have questioned its viability due to the skewed response distribution, non-stationarity between the training and test datasets, redundancy of examples and age of the data. However, use of KDD '99 is still prolific despite these issues, in part due to a lack of competing datasets, and the momentum of an extant body of research.

\quad In this report, we identify modern substitutes to KDD '99 and contrast performance on standard classification models: Neural Network, Support Vector Machine, Decision Tree, Random Forest, Naive Bayes. We discuss the applicability of the NSL-KDD and UNSW-NB15 datasets to the problem of Network Intrusion Detection.

}

\clearpage  % Abstract ended, start a new page
%% ----------------------------------------------------------------

\pagestyle{fancy}  %The page style headers have been "empty" all this time, now use the "fancy" headers as defined before to bring them back


%% ----------------------------------------------------------------
\lhead{\emph{Contents}}  % Set the left side page header to "Contents"
\tableofcontents  % Write out the Table of Contents

%% ----------------------------------------------------------------
\lhead{\emph{List of Figures}}  % Set the left side page header to "List if Figures"
\listoffigures  % Write out the List of Figures

%% ----------------------------------------------------------------
\lhead{\emph{List of Tables}}  % Set the left side page header to "List of Tables"
\listoftables  % Write out the List of Tables

%% ----------------------------------------------------------------
\setstretch{1.2}  % Set the line spacing to 1.5, this makes the following tables easier to read
\clearpage  % Start a new page
\lhead{\emph{Abbreviations}}  % Set the left side page header to "Abbreviations"
\listofsymbols{ll}  % Include a list of Abbreviations (a table of two columns)
{
% \textbf{Acronym} & \textbf{W}hat (it) \textbf{S}tands \textbf{F}or \\
\textbf{IDS}/\textbf{NIDS} & \textbf{N}etwork \textbf{I}ntrusion \textbf{D}etection \textbf{S}ystem \\
\textbf{NB} & \textbf{N}aive \textbf{B}ayes\\
\textbf{DT} & \textbf{D}ecision\textbf{T}rees\\
\textbf{RF} & \textbf{R}andom \textbf{F}orest\\
\textbf{GA} & \textbf{G}enetic \textbf{A}lgorithm\\
\textbf{SVM} & \textbf{S}upport \textbf{V}ector \textbf{M}achine\\
\textbf{NN}/\textbf{ANN} & \textbf{A}rtificial \textbf{N}eural \textbf{N}etwork\\
\textbf{PCA} & \textbf{P}rinciplel \textbf{C}omponent \textbf{A}nalysis\\
\textbf{FAR} & \textbf{F}ar \textbf{A}larm \textbf{R}ate\\
\textbf{DoS} & \textbf{D}enial \textbf{o}f \textbf{S}ervice\\
\textbf{U2R} & \textbf{U}ser \textbf{t}o \textbf{R}oot\\
\textbf{R2L} & \textbf{R}emote \textbf{t}o \textbf{L}ocal\\
\textbf{KDD} & \textbf{K}nowledge \textbf{D}iscovery \textbf{D}atabase\\
\textbf{MDA} & \textbf{M}ean \textbf{D}ecrease \textbf{A}ccuracy\\
\textbf{MDI} & \textbf{M}ean \textbf{D}ecrease \textbf{I}mpurity\\
}

%% ----------------------------------------------------------------
\iffalse %Hide constants page
\clearpage  % Start a new page
\lhead{\emph{Physical Constants}}  % Set the left side page header to "Physical Constants"
\listofconstants{lrcl}  % Include a list of Physical Constants (a four column table)
{
% Constant Name & Symbol & = & Constant Value (with units) \\
Speed of Light & $c$ & $=$ & $2.997\ 924\ 58\times10^{8}\ \mbox{ms}^{-\mbox{s}}$ (exact)\\

}
\fi %Close iffalse of constants page

%% ----------------------------------------------------------------
\iffalse %Hide symbols page
\clearpage  %Start a new page
\lhead{\emph{Symbols}}  % Set the left side page header to "Symbols"
\listofnomenclature{lll}  % Include a list of Symbols (a three column table)
{
% symbol & name & unit \\
$a$ & distance & m \\
$P$ & power & W (Js$^{-1}$) \\
& & \\ % Gap to separate the Roman symbols from the Greek
$\omega$ & angular frequency & rads$^{-1}$ \\
}
\fi %Close iffalse of symbols page
%% ----------------------------------------------------------------
% End of the pre-able, contents and lists of things
% Begin the Dedication page

\iffalse
\setstretch{1.3}  % Return the line spacing back to 1.3

\pagestyle{empty}  % Page style needs to be empty for this page
\dedicatory{For/Dedicated to/To my\ldots}

\addtocontents{toc}{\vspace{2em}}  % Add a gap in the Contents, for aesthetics
\fi
%% ----------------------------------------------------------------
\mainmatter	  % Begin normal, numeric (1,2,3...) page numbering
\pagestyle{fancy}  % Return the page headers back to the "fancy" style


% Change spacing above and below equations
\setlength{\abovedisplayskip}{0.75em}  %Source: http://tex.stackexchange.com/a/224989/110560
\setlength{\belowdisplayskip}{0.75em}


% Include the chapters of the thesis, as separate files
% Just uncomment the lines as you write the chapters

\input{Chapters/1_Introduction} % Introduction

\input{Chapters/2_Literature_Review} % Literature Review 

\input{Chapters/3_Present_Investigation} % Present Investigation

\input{Chapters/4_Design} % Design

%\input{Chapters/5_Preprocessing_and_Feature_Reduction} % Preprocessing and Feature Reduction

\input{Chapters/6_Implementation} % Implementation

\input{Chapters/7_Results} % Results and Discussion

\input{Chapters/8_Summary_and_Conclusion} % Summary and Conclusion

\input{Chapters/9_Future_Work} % Future Work


%% ----------------------------------------------------------------
% Now begin the Appendices, including them as separate files

\addtocontents{toc}{\vspace{2em}} % Add a gap in the Contents, for aesthetics

%\appendix % Cue to tell LaTeX that the following 'chapters' are Appendices

%\input{Appendices/AppendixA}	% Appendix Title

%\input{Appendices/AppendixB} % Appendix Title

%\input{Appendices/AppendixC} % Appendix Title

\addtocontents{toc}{\vspace{2em}}  % Add a gap in the Contents, for aesthetics
\backmatter

%% ----------------------------------------------------------------
\label{Bibliography}
\lhead{\emph{Bibliography}}  % Change the left side page header to "Bibliography"

\bibliographystyle{plain}  % Source: https://tex.stackexchange.com/a/89092/110560 
% Also see other styles: https://www.sharelatex.com/learn/Bibtex_bibliography_styles


\bibliography{Bibliography}  % The references (bibliography) information are stored in the file named "Bibliography.bib"

% Also see:
%  - How to correctly write authors:  tex.stackexchange.com/a/10773/110560
%  - All the types of BibTex entries: egr.uri.edu/ele/thesisguide/bibliography/#article   (the main types are article, techreport, online and misc)


\end{document}  % The End
%% ----------------------------------------------------------------

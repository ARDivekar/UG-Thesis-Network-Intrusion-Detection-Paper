\lhead{\emph{Results}}
\chapter{Results}

\quad\quad A sizeable collection of works base the performance of their methodology solely on the historical KDD'99 and its parent DARPA data. They are met by a smaller but more vocal set of authors decrying the use of such datasets, for a significant number of reasons. The critique of KDD has been going on for over a decade now, yet it remains prolific in research, with few authors choosing to work on newer, more relevant data. A small subset of authors such as Das et. al. have measured performance on more than one dataset.

\quad\quad Many of the cited studies achieve very high performance on DoS attacks (accuracy greater than 90\%), but fail quite miserably on U2R (often less than 10\% accuracy). The authors point out that this discrepancy is a result of the highly skewed classes in KDD.

\quad\quad A common theme is the need to eliminate false positives. While most research agrees that the false \textit{negative} rate is the primary criterion for evaluating IDSes - due to the very high cost associated with the inability to detect attacks - the false positive rate is a close second. The implicit agreement is that for IDS systems in a real-world setting, a network administrator must handle actual attacks if they arise, and a high false positive rate incurs the cost of human time spent hunting down errors.

\section{Results of Implementation:}

\newcommand{\numberOfColsOfImplementationTable}{\cline{1-4}}

%	\vspace{-3.5em}
%
	\quad\quad We begin our discussion with a simple table summarizing the various F-1 scores of each model. Note that for the SVM model, we reduced each dataset to either 15,000 or 25,000 examples.

	\quad\quad Table \ref{table: Multiclass results} compares the F1-scores of each model across the three multi-class datasets. We do not use accuracy as a metric for comparison as it does not capture the effect of skewed data in KDD and NSL-KDD. Instead, we use F1-score to compare the performance of each model.

%
	\vspace{1.5em}





\begin{longtabu} to \textwidth {| l | c | c | c |} 
		\caption{F-1 scores of each model, by dataset (using multiple classes)}
		\label{table: Multiclass results}\\
		\hline
							& KDD Test Set 	& NSL Test Set with SMOTE  	& UNSW-NB15	\\ \hline
		Decision Tree  		& 90\%			& 76\%						& 78\%		\\ \hline
		Random Forest  		& 90\%			& 76.5\%					& 77\% 		\\ \hline
		Neural Networks     & 91\%			& 75.5\%					& 74.2\%	\\ \hline
		Naive Bayes   		& 81\%			& 59\%						& 38.57\%	\\ \hline
		SVM (reduced)    	& 94\%			& 75.5\%					& 75\%		\\ \hline
		K-Means Clustering	& 90\%			& 75\%						& 70.86\%	\\ \hline
	\end{longtabu}
	
	\vspace{-2em}

	\quad\quad The results for KDD are the best among the three datasets due to the high support for each class. For the exact same classes in NSL, the scores are quite low. This can be attributed to lower support in NSL even with SMOTE. Hence it is difficult for classifiers for to distinguish between attacks. The UNSW-NB15 is a more balanced dataset compared to the other two. Yet the performance is poorer than the others, which can be attributed to the high number of classes in the dataset (10). Generally, higher the number of output classes, lower is the average performance of the classifier. Given this consideration, the classifier performs quite well.

	\vspace{2em}



	\begin{longtabu} to \textwidth {| l | c | c | c |}
		\caption{F-1 scores of each model, by dataset (using binary classes)}
		\label{table: Binary results}\\
		\hline
		& KDD Test Set 	& NSL Test Set with SMOTE  	& UNSW-NB15	\\ \hline
		Decision Tree  			& 94\% 	& 81\% 	& 86.5\%      \\ \hline
		Random Forest  			& 94\% 	& 81\% 	& 88.5\%      \\ \hline
		Neural Networks     & 94\% 	& 81\% 	& 84.88\%      \\ \hline
		Naive Bayes   			& 86\% 	& 78\% 	& 76\%      \\ \hline
		SVM (reduced)    		& 94\% 	& 82\% 	& 82\%      \\ \hline
		K-Means Clustering	& 93\% 	& 83\% 	& 83\%      \\ \hline
	\end{longtabu}

	\vspace{-1em}
	
	\quad\quad Table \ref{table: Binary results} comapres the performance of the datasets with binarized target classes (\textit{normal} and \textit{attack}). Again the results for KDD are the best among the three datasets due to the high support for each class. Even NSL with SMOTE is bottlenecked due to its lower support. The performance for KDD and NSL-KDD improves marginally when using binary rather than multi-class data, as some attacks are misclassified as attacks. Significantly, UNSW-NB15 shows a leap in performance compared to the other two, which can be attributed to the reduction in the number of target classes. Also since it has a high ratio of attack classes to normal (9:1), most of the attack examples are correctly categorized under the common 'attack' label. Hence, misclassification among the attack classes does not hamper accuracy.
	
	
	
	\vspace{2em}

	\quad\quad The tables that follow represent the results for each dataset (multi-class and binary) model-wise. The general trend of performance is more or less consistent for all the models.

	\quad\quad In multi-class KDD, the F1-score is very high for DoS, Probe and Normal while it is abysmal for U2R and R2L. The skewedness of data against U2R and R2L causes this (Largest-to-Smallest class ratio is 15631). The performance improves marginally in binary KDD as misclassification among attack classes is once again ignored.

	\quad\quad NSL-KDD, multiclass and binary, show the exact trends of KDD but with a decreased average accuracy and F1-score. The reduction in average accuracy is due to the low support of DoS, Probe and Normal in NSL-KDD. Hence, these are not learnt well causing a higher error rate.

	\quad\quad NSL-KDD with SMOTE and Random Undersampling, shows a slightly improved results for U2R and R2L. This improvement is due to the balanced nature of the dataset (Support for all classes is equal).

	\quad\quad UNSW-NB15 multi-class dataset has an above average performance with around 75\% F1-score. The F1-score is low due to the inherent nature of the data. The dataset has 10 output classes. Higher the number of output classes, lower is the average performance of the model. This is evident in UNSW-NB15 binary dataset, where the average F1-score goes well above 85\%.



\begin{enumerate}
	\setcounter{enumi}{0}	%% Source: http://tex.stackexchange.com/a/149/110560
	
	%% Source for \input{...} command: http://tex.stackexchange.com/a/250/110560
	\item \centering \textbf{Decision Tree:} \input{./Chapters/7_Results/Decision_Tree}
	
	\vspace{3em}
	
	\item \textbf{Random Forest:} \input{./Chapters/7_Results/Random_Forest}
	
	\vspace{3em}

	\item \textbf{Neural Networks:} \input{./Chapters/7_Results/Neural_Networks}
	
	\vspace{3em}

	\item \textbf{Naive Bayes:} \input{./Chapters/7_Results/Naive_Bayes}
	
	\vspace{3em}

	\item \textbf{Support Vector Machine:} \input{./Chapters/7_Results/Support_Vector_Machine}
	
	\vspace{3em}

	\item \textbf{K-Means Clustering:} \input{./Chapters/7_Results/KMeans_Clustering}


\end{enumerate}



\clearpage

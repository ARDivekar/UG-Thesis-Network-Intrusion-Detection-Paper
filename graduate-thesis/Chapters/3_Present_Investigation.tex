\lhead{\emph{Machine Learning Models}}
\chapter{Machine Learning Models}

\quad\quad In the following sections we discuss some of the most popular classifications models used in the literature for building a NIDS, viz. Naive Bayes, Decision Tree, Random Forest, Artificial Neural Networks, Support Vector Machine and K-Means Clustering.





\section{Naive Bayes}
\quad\quad Naive Bayes is a simplification of the Bayesian probability model. Despite its lack of flexibility, the classification problems solved using Naive Bayes often yield satisfactory results. This model serves as the baseline for the rest of the models.
Naive Bayes \cite{Reference2} determines the probability of an event given all evidences with the assumption that all the evidences are independent of each other and then determines the class using method of maximum  likelihood.

Probability of an example being of class $C_k$ given evidences $x_1, x_2, ..., x_n$ is:
\begin{equation}
P(C_k|x_1, x_2, ..., x_n) = \frac{P(C_k)P(x_1, x_2, ..., x_n|C_k)}{P(x_1, x_2, ..., x_n)}
\end{equation}

Naive Bayes assumes features to be independent of each other; the probability is thus given by:
\begin{equation}
P(C_k|x_1, x_2, ..., x_n) = \frac{P(C_k)P(x_1|C_k)P(x_2|C_k)...P(x_n|C_k)}{P(x_1, x_2, ..., x_n)} = \frac{P(C_k)\prod_{i=1}^{n}P(x_i|C_k)}{P(x_1, x_2, ..., x_n)}
\end{equation}

But since probability of evidence will be same,
\begin{equation}
P(C_k|x_1, x_2, ..., x_n)  \alpha  P(C_k)\prod_{i=1}^{n}P(x_i|C_k)
\end{equation}

Thus it is a problem of Maximum Aposterior Probability which is represented as:
\begin{equation}
\hat C = \argmax\limits_{C_k} P(C_k)\prod_{i=1}^{n}P(x_i|C_k)
\end{equation}

Thus the class that maximizes this probability will be the most appropriate label for the example.

\quad\quad To perform NB classification, we will first perform dimension reduction on the dataset removing the redundant and correlated featured. Once we get this new training data, we will calculate and store the conditional probability of all the features for each class. Then we will test this model with the test data. For each example, the class that gives the highest probability is the most likely label for that example. Then we will evaluate this model based on the obtained Precision and Recall or their single value metric F1 score.

\quad\quad Even with the assumption of independence, the model fares well on most classification problem. The error is due to 3 factors: Training data noise, bias and variance. Training data noise can be reduced with a good training data. Bias is the error in the data due to large groupings in the data. Variance is the error in the data due to small groupings in the data.
Also it is assumed that each feature follows a normal distribution with $\mu _i$ as mean and $\sigma _y$ as variance, thus the features follow: 
\begin{equation}
P(x_i|y) = \frac{1}{{\sigma _y\sqrt {2\pi } }}e^{{{ - \left( {x_i - \mu _i} \right)^2 } \mathord{\left/ {\vphantom {{ - \left( {x_i - \mu _i} \right)^2 } {2\sigma _y^2 }}} \right. \kern-\nulldelimiterspace} {2\sigma _y^2 }}}
\end{equation}




\section{Decision Tree}
\quad\quad In Decision trees \cite{Reference25}, the goal is to create a model that predicts the value of a target variable based on several input variables. A tree can be \textit{learned} by splitting the source set into subsets based on an attribute value test. This process is repeated on each derived subset in a recursive manner called recursive partitioning. The recursion is completed when the subset at a node has all the same value of the target variable or when the number of examples in a leaf reach that of the predetermined minimum. Each leaf of the tree is labeled with a class or a probability distribution over the classes. This process of top-down induction of decision trees (TDIDT) is an example of a greedy algorithm, and it is by far the most common strategy for learning decision trees from data.

\quad\quad Algorithms for constructing decision trees usually work top-down, by choosing a variable at each step that best splits the set of items. Different algorithms use different metrics for measuring "best". These generally measure the homogeneity of the target variable within the subsets. Some of them are:
\begin{description}
	\vspace{-1.5em} \item[$1)$] Gini impurity: Measure of how often a randomly chosen element from the set would be incorrectly labeled if it was randomly labeled according to the distribution of labels in the subset. It can be computed by summing the probability $f_{i}$ of an item with label $i$ being chosen times the probability $1-f_{i}$ of a mistake in categorizing that item.
	\begin{equation}
	I_{G}(f)=\sum _{i=1}^{J}f_{i}(1-f_{i})
	\end{equation}
	\vspace{-0.75em} \item[$2)$]  Information Gain: Based on the concept of entropy from information theory. Entropy is given by
	\begin{equation}
	I_{E}(f)=-\sum _{i=1}^{J}f_{i}\log _{2}^{}f_{i}
	\end{equation}
	Information Gain = Entropy(parent) - Weighted Sum of Entropy(Children)
    \begin{equation}
	IG(T,a)=H(T)-H(T|a)
	\end{equation}
	\begin{equation}
	IG(T,a) = H(T) - H
	\end{equation}
\end{description}

\quad\quad DTs are easy to interpret and perform automatic feature selection. They work well for large datasets and need no data preprocessing. But they always tend to overfit hence the need to prune the tree. Also the greedy approach of selecting features and split points is an approximation hence leading to local optima. The actual splitting problem is NP-Complete. Thus DTs are used to analyze the data and the relevant features rather than being used as the end classifier.




\section{Random Forest}
\quad\quad Random forests \cite{Reference26} are an ensemble learning method for classification that consists of many decision trees and outputs the class that is the mode of the classes output by individual trees. Random forests are collections of decision trees, all slightly different and are an improvement over a single decision tree. Unlike single decision trees which are likely to suffer from high variance or high Bias Random Forests use averaging to find a natural balance between the two extremes.

\quad\quad e.g. Consider a learning problem with 3 classes (0, 1, 2). Consider a trained random forest with 10 decision trees. When a new object has to be classified it is presented to each of these decision trees. Each tree will classify the object into one of the 3 classes. The output is (0, 2, 2, 2, 1, 2, 2, 1, 2, 2). Here the mode of the list (most frequently occurring value) is the class 2. Hence the output of the random forest is class 2.

\quad\quad Algorithm \ref{alg:random_forest_algo}. for random forests applies the general technique of bootstrap aggregating, or bagging, to tree learners. Given a training set $X = x_1, ..., x_n$ with responses $Y = y_1, ..., y_n$, bagging repeatedly (B times) selects a random sample with replacement of the training set and fits trees to these samples:

\begin{algorithm}
  \caption{Random Forest Training algorithm}
  \label{alg:random_forest_algo}
  \begin{algorithmic}[1]
       \State \texttt{$b \leftarrow 0$}
      \For{\texttt{$b < B$}}\Comment{B: number of trees}
        \State \texttt{Sample, with replacement, $n$ training examples from X, Y; call these $X_b$, $Y_b$}
        \Comment{X: Training examples,  Y: Responses}
        \State \texttt{Train a decision or regression tree fb on $X_b$, $Y_b$.}
      \EndFor
  \end{algorithmic}
\end{algorithm}

After training, predictions for unseen samples x' can be made by averaging the predictions from all the individual regression trees on x':
\begin{equation}
 {\hat {f}}={\frac {1}{B}}\sum _{b=1}^{B}{\hat {f}}_{b}(x')
\end{equation}

or by taking the majority vote in the case of decision trees.

\quad\quad This bootstrapping procedure leads to better model performance because it decreases the variance of the model, without increasing the bias. This means that while the predictions of a single tree are highly sensitive to noise in its training set, the average of many trees is not, as long as the trees are not correlated. Simply training many trees on a single training set would give strongly correlated trees (or even the same tree many times, if the training algorithm is deterministic); bootstrap sampling is a way of de-correlating the trees by showing them different training sets.

\quad\quad The number of samples/trees, B, is a free parameter. Typically, a few hundred to several thousand trees are used, depending on the size and nature of the training set. An optimal number of trees B can be found using cross-validation, or by observing the out-of-bag error: the mean prediction error on each training sample xᵢ, using only the trees that did not have xᵢ in their bootstrap sample. The training and test error tend to level off after some number of trees have been fit.




\section{Artificial Neural Networks}
\quad\quad Artificial Neural Networks, in their simplest form, can be considered an extension of the logistic regression model, encompassing  several different classifiers which combine outputs to make a prediction. The classifiers are organized and trained in \textit{layers}, each subdivided into one or more \textit{neural units} or \textit{activation units}. A typical classification model starts with the input layer corresponding to the predictors of our dataset, and concluding in a layer representing a number of zero-or-one outputs, one for each target class into which test examples can be classified. Only one of these outputs is triggered, and the example is thus classified into that target class. 

\quad\quad Between the input and output layers are a sequence of \textit{hidden layers}; the \textit{i'th} activation unit in the \textit{j'th} hidden layer (${1\leq j\leq}$ number of layers), takes as input the outputs of all the units of the previous layer, and applies a learned weighting function ${\theta_i^{(j)}}$, creating an output ${a_i^{(j)}}$ which it \textit{feeds forward} to each activation unit of the \textit{(j+1)'th} layer during prediction. This process repeats for all layers from the inputs to the vector of outputs. For the first layer, the values fed forward are the raw input values, and for the last layer, the activation unit outputs are the final outputs of the ANN.

\begin{figure}[!h]
	\centerline{\includegraphics[scale=0.1]{./images/neural_net.jpeg}}
	\caption{An Artifical Neural Network with one hidden layer, working on a dataset with three predictors and two target classes.}
	\label{fig: ANN}
\end{figure}

\hspace{7em} For each activation unit:
\begin{equation}
    a_i^{(j)} = g(z_i^{(j)})
\end{equation}
\hspace{7em} Where,
\begin{equation}
z_i^{(j)} = 
	\sum_{k=0}^{|\theta_i^{(j)}|}{ %Sum from k=0 to size of vector theta_i^(j)
		\theta_{i,k}^{(j)}{\cdot}a_k^{(j)}
	}
	, \hspace{1.5em} {z_i^{(j)}}{\in}\hspace{0.15em}{\rm I\!R}
\end{equation}


\hspace{7em} The \textit{sigmoid function} g(x) is: 
\begin{equation}
    g(x) = \frac{1}{1+e^{-x}}
\end{equation}

% \[ a_1^2 + a_2^2 = a_3^2 \] 

\quad\quad Each activation unit thus outputs a value ${a_i^{(j)}}$, calculated in a similar fashion to that of the Logistic Regression output. In vectorized Neural Network implementations, ${\theta^{(j)}}$ is a 2D matrix representing the mapping between layers \textit{j} and \textit{j+1}. The vector of all such matrices, ${\Theta}$, can thus be thought of as the trained neural network, which we 'apply' on each input example to predict an output.

\quad\quad A neural network model is most commonly trained by the \textit{Backpropagation algorithm}, where the weight of each unit is learned from the output layer to the input layer, in reverse order as to prediction. 


\quad\quad The number of hidden layers and neural units in each of them can be considered hyperparameters that must be tuned. If either of these hyperparameters is increased, so does the complexity of our net; bias is traded off for variance. Another point of note while training is that the net is able to learn higher-order combinations of the input features automatically, unlike linear and logistic regression models. While this does relieve the model-builder from the task of wantonly combining features, care must be taken to avoid overfitting, and regularization techniques may need to be applied.

\quad\quad Though they rank low in terms of interpretability (in the same league as Support Vector Machines), ANNs find wide application in image classification, text and musical analysis and robotics. This is especially so in recent years, as the advent of Deep Learning techniques coupled with NVIDIA Graphical Processing Units has been highly effective for training on large amounts of data to give excellent results.







\section{Support Vector Machine}

\quad\quad A Support Vector Machine (SVM) is a supervised learning model which may be used in both the  classification and regression settings. When dealing with binary classification, the decision boundary learned by an SVM is a linear separation of the target classes such that it has the \textit{largest margin} possible from the closest points in either class (where \textit{margin} is defined as the minimum distance of the boundary plane from any data point). This is called the \textit{maximum-margin hyperplane}, as in the general case, it may be a high dimensional hyperplane, for a \textit{p} dimensional input example.  


\begin{figure}[!h]
	\centerline{\includegraphics[scale=2.5]{./images/svm_optimal_hyperplane.png}}
	\caption{A linear maximum margin SVM, separating two target classes. Any other perfectly separating hyperplane produces a smaller margin \cite{Reference30}.}
	\label{fig: SVM}
\end{figure}

\quad\quad The SVM decision boundary intuitively accommodates good generalization characteristics; there is some 'room left', so to speak, on either side so as to allow unseen examples to come within the margin distance from the boundary while still ensuring their correct classification. A \textit{support vector} of an SVM is a set of points belonging to one of the classes, where each point is exactly within the margin distance from the optimal hyperplane. In the figure above, there are two points in the support vector for target \textit{(y = Red)}, and one for target \textit{(y = Blue)}.

\quad\quad A \textit{hard margin} SVM is one which tries to fit the data in a linearly separable order even in the event of outliers, and thus ends up overfitting to these outliers. A \textit{soft margin} classifier, on the  other hand, may miss a few outliers if it means giving a decision boundary. The hardness or softness of the classifier (formally, called the \textit{flex}), is a hyperparameter ${\lambda}$. For sufficiently small values of ${\lambda}$, the soft-margin SVM and hard-margin SVM behave identically.

\quad\quad For soft margin classifiers using the \textit{hinge loss} function, we try to minimize:
\begin{equation}
    \frac{1}{n}\sum_{i=0}^{n}{max(0, 1- y_i(\vec{w}\cdot \vec{x_i} - b))} + \hspace{0.5em} \lambda{||\vec{w}^2||}
\end{equation}

\hspace{7em} Where,

\vspace{-1em}

\hspace{10em} \textit{n} = the number of data points, 

\vspace{-1em}

\hspace{10em} \textit{${\vec{x_i}}$} = a particular data point, 

\vspace{-1em}

\hspace{10em} \textit{${\vec{w}}$} = the normal vector to the hyperplane, 

\vspace{-1em}

\hspace{10em} \textit{${\lambda}$} = the flex (used for regularization)


\quad\quad In general, many (if not most) datasets are not linearly separable. The \textit{kernel} of an SVM is the transformation function \textit{k(x,y)} of arbitrary complexity, which maps the non-linear data into a different, possibly higher dimensional space where it may be linearly separable. The applicability of kernel functions is limited to those following Mercer's theorem. 


\quad\quad The most basic kernel, the \textit{linear kernel}, is just a mapping of each data point to itself; the 'new' space is the same as the original. Other kernels include the polynomial and RBF, which have hyperparameters that may be used to tune their complexity.






\section{K-Means Clustering}

\quad\quad K-Means clustering is an \textit{unsupervised} learning algorithm which tries to organize all the data points into a number of clusters specified by the hyperparameter $K$. In unsupervised learning we don not provide the target values required; we only discern patterns in the data. 

Using K-Means for clustering data requires the following steps: 

\begin{enumerate}[noitemsep, topsep=-12pt]
	\item Data cleaning
	\item Determining the number of features (feature extraction)
	\item Determining the appropriate value of $K$
	\item Applying K-Means clustering on cleaned data with the appropriate number of features and with the value of $K$ determined above.
\end{enumerate}


\quad\quad In data cleaning we try to remove all the redundant data. Also our another aim is to remove data which is irrelevant, however it is generally advisable not to determine the relevancy of data just by logical inferences, so we leave this part to step two. Data cleaning also provides appropriate numeric values to all non-numeric data (codification). 

After assigning numeric values to all the data points, we normalize the data so that no one particular feature is assigned greater weightage towards the result because of its high numeric value. Some general methods of normalization include:
\begin{enumerate}[noitemsep, topsep=-12pt]
	\item Rescaling: 
		\begin{equation}
		x = \frac{x - min(x)}{max(x) - min(x)}
		\end{equation}
	\item Standardization:
		\begin{equation}
		x = \frac{x - \mu}{\sigma}
		\end{equation}
\end{enumerate}



\quad\quad After this we need to find out the data which contributes highest level of variation. Including all the features may lead to overfitting or may introduce unwanted noise. We use PCA for determining the features which have the greatest contribution towards the variance. We only select data features which contributes upto 0.8 variance in descending order. 

\quad\quad Another important parameter that needs to be resolved is the value of $K$. For some problems $K$ may be predefined; however if the clustering is performed on a random dataset which we know very little about, then this becomes an important issue. This can be resolved by using the \textit{elbow} or \textit{knee} method; we use different values of $K$ to achieve clustering and then plot the corresponding \textit{inertia} versus $K$.  \figurename{ \ref{fig: Knee Method}} is such an Elbow plot.

\begin{figure}[!h]
	\centerline{\includegraphics[scale=0.5]{./images/knee-method_kmeans.jpg}}
	\caption{Elbow method for selecting number of cluster ($K$) in K-Means clustering}
	\label{fig: Knee Method}
\end{figure}

\quad\quad Inertia is representative of the error associated with a particular clustering; less it better. In the figure knee or elbow is clearly distinguishable. The reason for using this method is that beyond the elbow the inertia does not significantly decrease with an increase in $K$; thus it adds to unnecessary computing.


\quad\quad Once $K$ is determined we applying K-Means clustering using different values of $K$ and finding out the inertia of the clustering in each case. Initially we select $K$ random centroids, then on the basis of the distance metric we assign points to each cluster. Some of the distance measures are: 
\begin{enumerate}[noitemsep, topsep=-12pt]
	\item Euclidean Distance:
		\begin{equation}
		d(p,c) = \sqrt{(p_{x} - c_{x})^{2} + (p_{y} - c{y})^2}
		\end{equation}
	\item Canberra Distance:
		\begin{equation}
		d(X, L) = \sum_{i=1}^{P}\frac{|X_{i} - L_{i}|}{X_{i} + L_{i}}
		\end{equation}
\end{enumerate}


\quad\quad Interia is determined by sum of distances of all points from the centroid of its respective cluster. After this step we take the mean, median or use some another criteria and assign another $K$ centroids. The steps are repeated to achieve the clusters and re-calculate the inertia. We repeat the process until the inertia becomes constant (Elbow method). We may repeat this entire clustering by reselecting random points as centroids. This serves to minimize the error that may creep via inappropriate starting centroids. The K-Means clustering algorithm is described in Algorithm \ref{alg:kmeans_algo}:
\begin{algorithm}
  \caption{K-Means clustering algorithm}
  \label{alg:kmeans_algo}
  \begin{algorithmic}[1]
       \State \texttt{Take k random points} \label{op1}
       \State \texttt{For each point assign point to nearest centroid} \label{op2}
       \State \texttt{After entire clustering reassign centroids on the basis of mean} \label{op3}
       \Repeat \label{op4}
       \State Steps \ref{op1} - \ref{op3}
       \Until{inertia of the system becomes constant} 
       \State Repeat steps \ref{op1} - \ref{op4} multiple times and use the best clustering amongst all

  \end{algorithmic}
\end{algorithm}

\quad\quad In the next chapter, we discuss how to use the various components of Machine Learning to build an IDS.

\clearpage



